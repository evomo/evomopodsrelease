Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name         = "EvomoMotionAI"
  s.version      = "1.0.39"
  s.summary      = "The Evomo MotionAI SDK allows to classify human movements in realtime."

  s.description  = <<-DESC
  
  The Evomo MotionAI SDK allows to classify human movements in realtime.
  
  DESC
  
  s.homepage     = "http://www.evomo.de"
  
  s.screenshots         = 'https://github.com/Evomo/evomoExampleApp/blob/master/Documentation/Media/ComplexMode.PNG'
  s.documentation_url   = 'https://github.com/Evomo/evomoExampleApp'
  
#  s.frameworks          = 'CoreML', 'CoreMotion', 'Accelerate', 'Darwin', 'simd', 'UIKit', 'HealthKit', 'Foundation'
  
  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license      = { :type => 'Commercial', :file => 'LICENSE' }

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author       = { "Evomo UG - Jakob Wowy" => "jakob.wowy@evomo.de" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.swift_version = "5.0"
  s.ios.deployment_target = "12.1"
#  s.watchos.deployment_target = "6.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source       = { :git => "https://bitbucket.org/evomo/evomomotionaibinary.git", :tag => "#{s.version}" }
  
  s.default_subspecs = "Basic"
  
  s.dependency "PromiseKit", "~> 6.1"
  s.dependency "PromiseKit/WatchConnectivity", "~> 6.1"
  s.dependency "Alamofire", "~> 4.0"
  s.dependency "SwiftyJSON", "~> 5.0.0"
  s.dependency "MessagePack.swift", "~> 3.0.0"
  
  # classification
  s.ios.dependency 'themis', '~> 0.12.2'

  
  # ―――----------- Binary ----------―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.subspec 'Movesense' do |ms|

      ms.ios.deployment_target = "12.1"

      ms.ios.vendored_frameworks = "Movesense/EvomoMotionAI.xcframework"

      ms.ios.dependency 'Movesense'

      # watch target deactivated
      # ms.watchos.vendored_frameworks = "#{s.name}Watch.framework"

  end
  
  s.subspec 'Basic' do |basic|

      basic.ios.deployment_target = "12.1"

      basic.ios.vendored_frameworks = "Basic/EvomoMotionAI.xcframework"
      
      basic.ios.dependency 'Movesense'

      # watch target deactivated
      # basic.watchos.vendored_frameworks = "#{s.name}Watch.framework"

  end

end

