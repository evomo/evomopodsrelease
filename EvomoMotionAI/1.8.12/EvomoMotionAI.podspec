Pod::Spec.new do |s|

    # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name         = "EvomoMotionAI"
  s.version      = "1.8.12"
  s.summary      = "The Evomo MotionAI SDK allows to classify human movements in realtime."

  s.description  = <<-DESC
  
  The Evomo MotionAI SDK allows to classify human movements in realtime.
  
  DESC
  
  s.homepage     = "http://www.evomo.de"
  
  s.screenshots         = 'https://github.com/Evomo/evomoExampleApp/blob/master/Documentation/Media/ComplexMode.PNG'
  
  s.documentation_url   = 'https://evomo.github.io/motionAI-docu/'
  
  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license      = { :type => 'Commercial', :file => 'Basic/EvomoMotionAI.xcframework/LICENSE' }

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author       = { "Jakob Wowy" => "jakob.wowy@evomo.de" }


  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.ios.deployment_target = "12.1"
  s.watchos.deployment_target = "6.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source       = { :git => "https://bitbucket.org/evomo/evomomotionaibinary.git", :tag => "#{s.version}" }
  
  s.default_subspecs = "Basic"
  
  s.dependency "PromiseKit", "~> 6.1"
  s.dependency "PromiseKit/WatchConnectivity", "~> 6.1"
  s.dependency "Alamofire", "~> 4.0"
  s.dependency "SwiftyJSON", "~> 5.0.0"
  s.dependency "MessagePack.swift", "~> 3.0.0"
  s.dependency "AWSS3"
  
  # classification
  s.ios.dependency 'themis', '~> 0.12.2'
  
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  # ―――----------- Binary ----------―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.subspec 'Movesense' do |ms|
      
      ms.ios.deployment_target = "12.1"
      
      ms.ios.vendored_frameworks = "Movesense/EvomoMotionAIMovesense.xcframework"
      
      ms.ios.dependency 'Movesense'
      
  end
  
  s.subspec 'Basic' do |basic|

      basic.ios.deployment_target = "12.1"

      basic.ios.vendored_frameworks = "Basic/EvomoMotionAI.xcframework"

  end
  
  s.subspec 'ClassificationOnly' do |basic|

       basic.ios.deployment_target = "12.1"

       basic.ios.vendored_frameworks = "ClassificationOnly/EVOClassification.xcframework"

   end
 
end

