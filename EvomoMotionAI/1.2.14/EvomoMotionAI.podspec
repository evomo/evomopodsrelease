Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name         = "EvomoMotionAI"
  s.version      = "1.2.14"
  s.summary      = "The Evomo MotionAI SDK allows to classify human movements in realtime."

  s.description  = <<-DESC

The Evomo MotionAI SDK allows to classify human movements in realtime..

                   DESC

  s.homepage     = "http://www.evomo.de"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license      = { :type => 'Commercial', :file => 'Basic/EvomoMotionAI.xcframework/LICENSE' }

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author       = { "Jakob Wowy" => "jakob.wowy@evomo.de" }


  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.swift_version = "5.0"
  s.ios.deployment_target = "12.1"
  s.watchos.deployment_target = "6.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source       = { :git => "https://bitbucket.org/evomo/evomomotionaibinary.git", :tag => "#{s.version}" }
  
  s.default_subspecs = "Basic"
  
  s.dependency "PromiseKit", "~> 6.1"
  s.dependency "PromiseKit/WatchConnectivity", "~> 6.1"
  s.dependency "Alamofire", "~> 4.0"
  s.dependency "SwiftyJSON", "~> 5.0.0"
  s.dependency "MessagePack.swift", "~> 3.0.0"
  
  # classification
  s.ios.dependency'themis', '~> 0.12.2'

  
  # ―――----------- Binary ----------―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.subspec 'Movesense' do |ms|
      
      ms.ios.deployment_target = "12.1"
      
      ms.ios.vendored_frameworks = "Movesense/EvomoMotionAI.xcframework"
      
      ms.ios.dependency 'Movesense'
      
  end
  
  s.subspec 'Basic' do |basic|

      basic.ios.deployment_target = "12.1"

      basic.ios.vendored_frameworks = "Basic/EvomoMotionAI.xcframework"

  end
 
end

